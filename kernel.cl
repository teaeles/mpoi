/*26:*/
#line 794 "./mpoi.w"

kernel void vec_calc(global const float*a,
global const float*b,
global float*result
){

int id= get_global_id(0);

#undef __USE_CONDITIONAL__

#ifdef __USE_CONDITIONAL__

if(sin(a[id]*cos(b[id]))> 0.){
result[id]= exp(sin(a[id])*cos(b[id])+cos(a[id])*sin(b[id]));
}else{
result[id]= exp(sin(a[id])*cos(b[id])-cos(a[id])*sin(b[id]));
}

#else

result[id]= exp(sin(a[id])*cos(b[id])
+sign(sin(a[id])*cos(b[id]))*cos(a[id])*sin(b[id]));

#endif
}









/*:26*/
