/*25:*/
#line 672 "./mpoi.w"

#include "mpoi.h"
#include <iostream> 
#include <chrono> 
#include <cmath> 

using namespace std;
using namespace std::chrono;

int main(int argc,char*argv[]){
mpoi pc("./kernel.cl");
pc.display_platform_info();

size_t kernel_id= pc.create_kernel("vec_calc");

const size_t size= 5000000;
float*a= (float*)new float[size];
float*b= (float*)new float[size];
float*c= (float*)new float[size];

for(size_t i= 0;i!=size;i++){
a[i]= i;
b[i]= size-i;
}

const unsigned num_trials= 10;
vector<int> duration_parallel(num_trials);
vector<int> duration_serial(num_trials);
vector<float> difference_parallel_serial(num_trials);

for(unsigned i= 0;i!=num_trials;i++){
auto t0= high_resolution_clock::now();

size_t a_buffer= pc.create_buffer(mpoi::buffer_property::READ_ONLY,
size*sizeof(float));
size_t b_buffer= pc.create_buffer(mpoi::buffer_property::READ_ONLY,
size*sizeof(float));
size_t c_buffer= pc.create_buffer(mpoi::buffer_property::READ_WRITE,
size*sizeof(float));

pc.enqueue_write_buffer(a_buffer,size*sizeof(float),a);
pc.enqueue_write_buffer(b_buffer,size*sizeof(float),b);

pc.set_kernel_argument(kernel_id,0,a_buffer);
pc.set_kernel_argument(kernel_id,1,b_buffer);
pc.set_kernel_argument(kernel_id,2,c_buffer);

pc.enqueue_data_parallel_kernel(kernel_id,size,100);

pc.enqueue_read_buffer(c_buffer,size*sizeof(float),c);

pc.release_buffer(a_buffer);
pc.release_buffer(b_buffer);
pc.release_buffer(c_buffer);

auto t1= high_resolution_clock::now();
duration_parallel[i]= static_cast<int> 
(duration_cast<milliseconds> (t1-t0).count());

float*d= (float*)new float[size];
t0= high_resolution_clock::now();
for(size_t j= 0;j!=size;j++){
if(sin(a[j])*cos(b[j])> 0.f){
d[j]= exp(sin(a[j])*cos(b[j])+cos(a[j])*sin(b[j]));
}else{
d[j]= exp(sin(a[j])*cos(b[j])-cos(a[j])*sin(b[j]));
}
}
t1= high_resolution_clock::now();
duration_serial[i]= static_cast<int> 
(duration_cast<milliseconds> (t1-t0).count());

float diff= 0.f;
for(size_t j= 0;j!=size;j++){
diff+= pow(c[j]-d[j],2);
}
diff/= float(size);
difference_parallel_serial[i]= diff;

delete[]d;
}

cout<<"================ S U M M A R Y ================\n\n";

float sum_ratio= 0.f;
int sum_parallel= 0;
int sum_serial= 0;
cout<<"\tParallel"<<"\t\tSerial"<<"\t\tRatio (P/S)"
<<"\t\tDifference\n";
cout<<"  ----------------------------------------------------------------\n";
for(unsigned i= 0;i!=num_trials;i++){
cout<<"\t"<<duration_parallel[i]<<" msec"
<<"\t\t"<<duration_serial[i]<<" msec"
<<"\t\t"<<float(duration_parallel[i])/float(duration_serial[i])
<<"\t\t"<<difference_parallel_serial[i]
<<endl;
sum_parallel+= duration_parallel[i];
sum_serial+= duration_serial[i];
sum_ratio+= float(duration_parallel[i])/float(duration_serial[i]);
}
cout<<"  ----------------------------------------------------------------\n";
cout<<"\t"<<float(sum_parallel)/float(num_trials)<<" msec"
<<"\t\t"<<float(sum_serial)/float(num_trials)<<" msec"
<<"\t\t"<<sum_ratio/float(num_trials)
<<endl;

delete[]a;
delete[]b;
delete[]c;


return 0;
}




/*:25*/
