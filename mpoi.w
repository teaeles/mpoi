% ---------------------------------------------------------------------------- %
%                                                                              %
%                                 PROGRAM.W                                    %
%                                                                              %
%                                                                              %
% ---------------------------------------------------------------------------- %

% ---------------------------------------------------------------------------- %
%                                                                              %
%                                  IN LIMBO                                    %
%                                                                              %
% ---------------------------------------------------------------------------- %

% This document will be typeset in Korean
\input kotexplain

% Definitions of fonts
\font\biglogo=cmss10 scaled\magstep2
\font\authorfont=cmr12
\font\linefont=cmss8
\font\scaps=cmcsc10
\font\logo=cmss10
\font\ninerm=cmr9
\let\mc=\ninerm % medium caps

% Definitions of names
\def\apple{{\mc Apple}}
\def\macosx{{\mc OS X\spacefactor1000}}
\def\windows{{\mc Windows}}
\def\boost{{\mc BOOST\spacefactor1000}}
\def\gsl{{\mc GSL\spacefactor1000}}
\def\llvm{{\mc LLVM\spacefactor1000}}

% Definitions of mathematical notations
\def\trans{^\top}
\def\inv{^{-1}}

% Definitions of useful commands
\def\example{{\bf Example.\ }}
\def\myitem{\item{$\bullet$}}

% Definition for current date
\def\today{\ifcase\month\or
	January\or February\or March\or April\or May\or June\or
	July\or August\or September\or October\or November\or December\fi
	\space\number\day, \number\year}

% Definitions for revisiondata and title
\def\years{2016}
\def\title{MPOI}
\def\headertitle{Multi-Processing Object Interface}

% Definitions for left/right headers, top/bottom of table of contents material
% \def\lheader{\mainfont\the\pageno\kern1pc(\topsecno)\eightrm
%   \qquad\grouptitle\hfill\headertitle}
% \def\rheader{\eightrm\headertitle\hfill\grouptitle\qquad\mainfont
%   (\topsecno)\kern1pc\the\pageno}

\def\topofcontents{\null\vfill
  \centerline{\titlefont MPOI: Multi-Processing Object Interface}
  \vskip15pt
  \centerline{(Last revised on \today)}
  \vfill}
\def\botofcontents{\vfill
  \noindent
  Copyright \copyright\ \years~by Changmook Chun
  \bigskip\noindent
  This document is published by Changmook Chun.  All rights reserved.
  No part of this publication may be reproduced, stored in a retrieval systems,
  or transmitted, in any form or by any means, electronic, mechanical, photocopying,
  recording, or otherwise, without the prior written permission of the author.
}

% Print line numbers on the left side of C++ code.
\newcount\linenum \linenum=0
\def\6{\ifmmode\else\par\hangindent\ind em\noindent
  \hbox to 0pt{\hss\global\advance\linenum by 1
    \linefont\the\linenum\hskip 7mm}
  \hangindent\ind em\noindent\kern\ind em\copy\bakk\ignorespaces\fi}


% Modification to CWEBMAC
\secpagedepth=3
\def\9#1#2{\&{#1}::\\{#2}}

% Make title page with a blank page behind it for a printer with a duplex.
% \null\vfill
% \centerline{\titlefont The {\biglogo CLAT} Library}
% \vskip 18pt\centerline{(Last revised on \today)}
% \vskip 24pt
% \centerline{\authorfont Changmook Chun}
% \vfill
% \titletrue\eject\hbox to 0pt{}
% \pageno=0 \titletrue\eject

% Some useful commands.
\def\newline{\vskip\baselineskip}
\def\header#1{\hbox{\tt #1.h}}


% ---------------------------------------------------------------------------- %

% Format
@s std int
@s string int
@s ostream int
@s istream int


\datethis

@* Introduction.  이 문서는 OpenCL을 이용한 병렬 연산을 지원하는 라이브러리를 설명한다.




@* Declaration.

@(mpoi.h@>=
#ifndef __MULTI_PROCESSING_OBJECT_INTERFACE_H_
#define __MULTI_PROCESSING_OBJECT_INTERFACE_H_

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <iterator>

using namespace std;

class mpoi {
  @<Constants of |mpoi|@>@;
  @<Data members of |mpoi|@>@;
  @<Member functions of |mpoi|@>@;
};

#endif




@ OpenCL을 이용한 multi-processing interface를 정의하기 위하여 다음과 같은
멤버 변수들이 필요하다.

@s cl_device_id int
@s cl_context int
@s cl_command_queue int
@s cl_program int
@s cl_kernel int

@<Data members of |mpoi|@>=
protected:@/
cl_device_id _device_id;
cl_context _context;
cl_command_queue _cmd_queue;
cl_program _program;
vector<cl_kernel> _kernels;
map<size_t,cl_mem> _buffers;
size_t _next_key;
string _src;




@* Implementation.

@(mpoi.cpp@>=
#include "mpoi.h"

@<Constructors and destructor of |mpoi|@>@;
@<Methods to handle |mpoi| object@>@;
@<Setup and cleanup of OpenCL@>@;
@<Method to build a kernel@>@;
@<Methods to display platform info@>@;
@<Methods to handle device memory buffers@>@;
@<Methods to set kernel arguments@>@;
@<Methods to execute a kernel@>@;




@ |mpoi| 객체를 다루기 위한 methods.

@<Methods to handle |mpoi| object@>=
mpoi& mpoi::operator= (const mpoi& obj) @+ {
  _next_key = 0;
  _src = obj._src;

  _setup_opencl ();

  if (_src != "") {
    build_program (_src);
  }

  return *this;
}

@ @<Member functions of |mpoi|@>+=
public:@/
mpoi& operator= (const mpoi&);




@ OpenCL 사용을 위한 기본 설정과 사용 후 정리.

@s cl_int int
@s cl_uint int
@s cl_platform_id int

@<Setup and cleanup of OpenCL@>=
void mpoi::_setup_opencl () @+ {
  cl_uint num_platforms;
  cl_int err = clGetPlatformIDs (0, NULL, &num_platforms);
  if (err != CL_SUCCESS || num_platforms < 1) {
    cerr << "Failed to find any OpenCL platforms.\n";
    exit (1);
  }

  cl_platform_id* platformIDs
    = (cl_platform_id*)new cl_platform_id[num_platforms];
  err = clGetPlatformIDs (num_platforms, platformIDs, NULL);
  if (err != CL_SUCCESS) {
    cerr << "Failed to find any OpenCL platforms.\n";
    exit (1);
  }
  cout << num_platforms << " OpenCL platform(s) found.\n";

  bool device_found =false;
  cl_uint current_max_compute_units =0;
  cl_uint max_compute_units =0;

  for (cl_uint i=0; i!= num_platforms; i++) {
    cout << "Platform # " << i << endl;
    cl_uint num_devices;
    err = clGetDeviceIDs (platformIDs[i], CL_DEVICE_TYPE_GPU,
                          0, NULL, &num_devices);
    if (num_devices < 1) {
      cerr << "No GPU devices found for platform " << platformIDs[i] << endl;
    } else {
      device_found =true;
      cout << num_devices << " GPU device(s) found for platform "
           << platformIDs[i] << endl;
      cl_device_id* deviceIDs = (cl_device_id*)new cl_device_id[num_devices];
      for (cl_uint j=0; j!=num_devices; j++) {
        err = clGetDeviceIDs (platformIDs[i], CL_DEVICE_TYPE_GPU, 1,
                              &deviceIDs[j], NULL);
        cl_uint device_vendor_id;
        err = clGetDeviceInfo (deviceIDs[j], CL_DEVICE_VENDOR_ID,
                               sizeof(cl_uint), &device_vendor_id, NULL);
        cout << "Device vendor ID: " << device_vendor_id << endl;
        err = clGetDeviceInfo (deviceIDs[j], CL_DEVICE_MAX_COMPUTE_UNITS,
                               sizeof(cl_uint), &max_compute_units, NULL);
        cout << "Device has " << max_compute_units << " compute units.\n";
        if (max_compute_units > current_max_compute_units) {
          current_max_compute_units = max_compute_units;
          _device_id = deviceIDs[j];
        }
      }
      delete[] deviceIDs;
    }
  }

  if (!device_found) {
    cerr << "No OpenCL GPU devices found through all the platforms.\n";
    exit (1);
  }

  _context = clCreateContext (NULL, 1, &_device_id, NULL, NULL, &err);
  if (err!=CL_SUCCESS) {
    cerr << "Error in creating a context.\n";
    exit (1);
  }
  _cmd_queue = clCreateCommandQueue (_context, _device_id, 0, &err);
  if (err!=CL_SUCCESS) {
    cerr << "Error in creating a command queue.\n";
    exit (1);
  }
  _program = NULL;

  delete[] platformIDs;
}

void mpoi::_cleanup_opencl () @+ {
  clFlush (_cmd_queue);
  clFinish (_cmd_queue);
  for (size_t i=0; i!=_kernels.size(); i++) {
    clReleaseKernel (_kernels[i]);
  }
  if (!_program) {
    clReleaseProgram (_program);
  }
  clReleaseCommandQueue (_cmd_queue);
  clReleaseContext (_context);
}

@ @<Member functions of |mpoi|@>+=
private:@/
void _setup_opencl();
void _cleanup_opencl();




@ Kernel을 생성하는 method.

@s ifstream int
@s istreambuf_iterator int

@<Method to build a kernel@>=
void mpoi::build_program (const string& src_file) @+ {
  ifstream in(src_file);
  if (in.is_open()) {
    string src ((istreambuf_iterator<char>(in)), istreambuf_iterator<char>());
    const char* src_string = src.c_str();
    const size_t src_length = src.length();
    cl_int err;

    _program = clCreateProgramWithSource (_context, 1,
                                          (const char **)&src_string,
                                          (const size_t *)&src_length,
                                          &err);
    if (err!=CL_SUCCESS) {
      cerr << "Error in creating a program.\n";
    }

    err= clBuildProgram(_program,1,&_device_id,NULL,NULL,NULL);
    @<Check kernel program build error@>;

  } else {
    exit (1);
  }
}

size_t mpoi::create_kernel (const string& name) @+ {
  size_t id = _kernels.size();
  cl_int err;
  _kernels.push_back (clCreateKernel (_program, name.c_str(), &err));
  return id;
}

@ @<Member functions of |mpoi|@>+=
public:@/
void build_program (const string&);
size_t create_kernel (const string&);




@ Kernel program의 build 결과를 확인하고, 오류가 있으면 build log를 출력한다.

@<Check kernel program build error@>=
if (err!=CL_SUCCESS) {
  cerr << "Error in building a program.\n";
  cl_build_status build_status;

  clGetProgramBuildInfo(_program, _device_id,
                        CL_PROGRAM_BUILD_STATUS,
                        sizeof(cl_build_status),
                        &build_status, NULL);

  if (build_status != CL_SUCCESS) {
    size_t ret_val_size;
    clGetProgramBuildInfo(_program, _device_id,
                          CL_PROGRAM_BUILD_LOG, 0,
                          NULL, &ret_val_size);

    char* build_log = (char*)new char[ret_val_size+1];

    clGetProgramBuildInfo(_program, _device_id,
                          CL_PROGRAM_BUILD_LOG, ret_val_size,
                          build_log, NULL);

    build_log[ret_val_size] = '\0';

    cerr << "BUILD LOG: " << build_log << endl;

    delete[] build_log;
  }
}




@ 생성자와 소멸자.

@<Constructors and destructor of |mpoi|@>=
mpoi::mpoi ()
: _next_key(0),
  _src("")
{
  _setup_opencl ();
}

mpoi::mpoi (const string& src)
: _next_key(0),
  _src (src)
{
  _setup_opencl();
  build_program (_src);
}

mpoi::mpoi (const mpoi& obj)
: _device_id (obj._device_id),
  _context (obj._context),
  _cmd_queue (obj._cmd_queue),
  _program (obj._program),
  _kernels (obj._kernels),
  _buffers (obj._buffers),
  _next_key (obj._next_key),
  _src (obj._src)
{}

mpoi::~mpoi () @+ {
  _cleanup_opencl();
}

@ @<Member functions of |mpoi|@>+=
public:@/
mpoi ();
mpoi (const string&);
mpoi (const mpoi&);
virtual ~mpoi ();




@ OpenCL platform에 관한 정보를 보여주기 위한 methods.

@s cl_platform_info int

@<Methods to display platform info@>=
void mpoi::display_platform_info () const @+ {
  cl_uint num_platforms;
  cl_int err = clGetPlatformIDs (0, NULL, &num_platforms);
  if (err != CL_SUCCESS || num_platforms < 1) {
    cerr << "Failed to find any OpenCL platforms.\n";
    return;
  }

  cl_platform_id* platformIDs
    = (cl_platform_id *)new cl_platform_id[num_platforms];

  err = clGetPlatformIDs (num_platforms, platformIDs, NULL);
  if (err != CL_SUCCESS) {
    cerr << "Failed to find any OpenCL platforms.\n";
    return;
  }
  cout << "Number of platforms: \t" << num_platforms << endl;

  for (cl_uint i=0; i!=num_platforms; i++) {
    _display_platform_info (platformIDs[i], CL_PLATFORM_PROFILE,
                            "CL_PLATFORM_PROFILE   ");
    _display_platform_info (platformIDs[i], CL_PLATFORM_VERSION,
                            "CL_PLATFORM_VERSION   ");
    _display_platform_info (platformIDs[i], CL_PLATFORM_VENDOR,
                            "CL_PLATFORM_VENDOR    ");
    _display_platform_info (platformIDs[i], CL_PLATFORM_EXTENSIONS,
                            "CL_PLATFORM_EXTENSIONS");
  }

  delete[] platformIDs;
}

void
mpoi::_display_platform_info (cl_platform_id id,
                              cl_platform_info name,
                              string str
                              ) const @+ {
  size_t param_value_size;
  cl_int err = clGetPlatformInfo (id, name, 0, NULL, &param_value_size);
  if (err != CL_SUCCESS) {
    cerr << "Failed to find OpenCL platform " << str << ".\n";
    return;
  }

  char* info = (char*)new char[param_value_size];
  err = clGetPlatformInfo (id, name, param_value_size, info, NULL);
  if (err != CL_SUCCESS) {
    cerr << "Failed to find OpenCL platform " << str << ".\n";
    return;
  }
  cout << "\t" << str << ":\t" << info << endl;

  delete[] info;
}

@ @<Member functions of |mpoi|@>+=
public:@/
void display_platform_info () const;
protected:@/
void _display_platform_info (cl_platform_id, cl_platform_info, string) const;




@ OpenCL device에 buffer 메모리를 생성할 때 지정할 속성에 관한 상수들을 정의한다.

@<Constants of |mpoi|@>+=
public:@/
enum buffer_property {
  READ_ONLY = CL_MEM_READ_ONLY,
  WRITE_ONLY = CL_MEM_WRITE_ONLY,
  READ_WRITE = CL_MEM_READ_WRITE
};

@ OpenCL device에 메모리를 생성하고 해제하기 위한 method들을 정의한다.

@<Methods to handle device memory buffers@>+=
size_t mpoi::create_buffer (mpoi::buffer_property bp,
                            const size_t sz
                            ) @+ {

  cl_int err;
  cl_mem buffer = clCreateBuffer (_context, bp, sz, NULL, &err);
  _buffers[_next_key] = buffer;
  _next_key++;
  return _next_key -1;
}

void mpoi::release_buffer (const size_t id) @+ {
  if (_buffers[id] != NULL) {
    clReleaseMemObject (_buffers[id]);
    _buffers[id] = NULL;
  }
}

@ @<Member functions of |mpoi|@>+=
public:@/
size_t create_buffer (mpoi::buffer_property, const size_t);
void release_buffer (const size_t);




@ OpenCL device의 buffer에 host의 메모리 내용을 기록하거나, 반대로
buffer의 내용을 host의 메모리로 읽어오기 위한 method.

@<Methods to handle device memory buffers@>+=
void mpoi::enqueue_write_buffer (const size_t id,
                                 const size_t size,
                                 const void* mem
                                 ) @+ {
  if (_buffers[id] != NULL) {
    cl_int err = clEnqueueWriteBuffer (_cmd_queue,
                                       _buffers[id],
                                       CL_TRUE, 0, size, mem,
                                       0, NULL, NULL);
    if (err != CL_SUCCESS) {
      cerr << "Error in enqueuing a write buffer.\n";
      return;
    }
  }
}

void mpoi::enqueue_read_buffer (const size_t id,
                                const size_t size,
                                void* mem
                                ) @+ {
  if (_buffers[id] != NULL) {
    cl_int err = clEnqueueReadBuffer (_cmd_queue,
                                      _buffers[id],
                                      CL_TRUE, 0, size, mem,
                                      0, NULL, NULL);
    if (err != CL_SUCCESS) {
      cerr << "Error in enqueuing a read buffer.\n";
      return;
    }
  }
}

@ @<Member functions of |mpoi|@>+=
public:@/
void enqueue_write_buffer (const size_t, const size_t, const void*);
void enqueue_read_buffer (const size_t, const size_t, void*);




@ OpenCL kernel의 인자를 설정하기 위한 method.

@<Methods to set kernel arguments@>+=
void mpoi::set_kernel_argument (const size_t kernel_id,
                                const size_t order,
                                const size_t buffer_id
                                ) @+ {
  if ((_kernels[kernel_id] != NULL) && (_buffers[buffer_id] != NULL)) {
    cl_int err = clSetKernelArg (_kernels[kernel_id],
                                 static_cast<cl_uint>(order), sizeof(cl_mem),
                                 (void*)&(_buffers[buffer_id]));
    if (err != CL_SUCCESS) {
      cerr << "Error in setting a kernel argument!\n";
      return;
    }
  }
}

void mpoi::set_kernel_argument (const size_t id,
                                const size_t order,
                                const size_t size,
                                void* mem
                                ) @+ {
  if (_kernels[id]!=NULL) {
    cl_int err = clSetKernelArg (_kernels[id],
                                 static_cast<cl_uint>(order), size, mem);

    if (err != CL_SUCCESS) {
      cerr << "Error in setting a kernel argument!\n";
      return;
    }
  }
}

@ @<Member functions of |mpoi|@>+=
public:@/
void
set_kernel_argument (const size_t, const size_t,
                     const size_t);

void
set_kernel_argument (const size_t, const size_t, const size_t,
                     void*);




@ Data 병렬 처리를 위한 OpenCL kernel을 실행하는 method.

@<Methods to execute a kernel@>+=
void mpoi::enqueue_data_parallel_kernel (const size_t id,
                                         size_t num_global_items,
                                         size_t num_local_items
                                         ) @+ {
  if (_kernels[id] != NULL) {
    for (;num_local_items!=1; num_local_items--) {
      if (num_global_items%num_local_items ==0) break;
    }
    cout << "Local item size = " << num_local_items << endl;
    cl_int err = clEnqueueNDRangeKernel (_cmd_queue,
                                         _kernels[id],
                                         1, NULL,
                                         &num_global_items,
                                         &num_local_items,
                                         0, NULL, NULL);
    if (err != CL_SUCCESS) {
      cerr << "Error in enqueuing nd range kernel.\n";
      return;
    }
  }
}

@ @<Member functions of |mpoi|@>+=
public:@/
void enqueue_data_parallel_kernel (const size_t, size_t, size_t);




@* Test.

@s chrono int
@s high_resolution_clock int
@s duration_cast static_cast
@s milliseconds int

@(test.cpp@>=
#include "mpoi.h"
#include <iostream>
#include <chrono>
#include <cmath>

using namespace std;
using namespace std::chrono;

int main (int argc, char* argv[]) @+ {
  mpoi pc("./kernel.cl");
  pc.display_platform_info ();

  size_t kernel_id = pc.create_kernel ("vec_calc");

  const size_t size = 5000000;
  float* a = (float *)new float[size];
  float* b = (float *)new float[size];
  float* c = (float *)new float[size];

  for (size_t i=0; i!=size; i++) {
    a[i] = i;
    b[i] = size-i;
  }

  const unsigned num_trials = 10;
  vector<int> duration_parallel (num_trials);
  vector<int> duration_serial (num_trials);
  vector<float> difference_parallel_serial (num_trials);

  for (unsigned i=0; i!=num_trials; i++) {
    auto t0 = high_resolution_clock::now(); // Parallel computation begins here.

    size_t a_buffer = pc.create_buffer (mpoi::buffer_property::READ_ONLY,
                                        size*sizeof(float));
    size_t b_buffer = pc.create_buffer (mpoi::buffer_property::READ_ONLY,
                                        size*sizeof(float));
    size_t c_buffer = pc.create_buffer (mpoi::buffer_property::READ_WRITE,
                                        size*sizeof(float));

    pc.enqueue_write_buffer (a_buffer, size*sizeof(float), a);
    pc.enqueue_write_buffer (b_buffer, size*sizeof(float), b);

    pc.set_kernel_argument (kernel_id, 0, a_buffer);
    pc.set_kernel_argument (kernel_id, 1, b_buffer);
    pc.set_kernel_argument (kernel_id, 2, c_buffer);

    pc.enqueue_data_parallel_kernel (kernel_id, size, 100);

    pc.enqueue_read_buffer (c_buffer, size*sizeof(float), c);

    pc.release_buffer (a_buffer);
    pc.release_buffer (b_buffer);
    pc.release_buffer (c_buffer);

    auto t1 = high_resolution_clock::now(); // Parallel computation ends here.
    duration_parallel[i] = static_cast<int>
                           (duration_cast<milliseconds>(t1-t0).count());

    float* d = (float *)new float[size];
    t0 = high_resolution_clock::now(); // Serial computation begins here.
    for (size_t j=0; j!=size; j++) {
      if (sin(a[j])*cos(b[j]) > 0.f) {
        d[j] = exp(sin(a[j])*cos(b[j]) +cos(a[j])*sin(b[j]));
      } else {
        d[j] = exp(sin(a[j])*cos(b[j]) -cos(a[j])*sin(b[j]));
      }
    }
    t1 = high_resolution_clock::now(); // Serial computation ends here.
    duration_serial[i] = static_cast<int>
                         (duration_cast<milliseconds>(t1-t0).count());

    float diff = 0.f;
    for (size_t j=0; j!=size; j++) {
      diff += pow(c[j]-d[j],2);
    }
    diff /= float(size);
    difference_parallel_serial[i] = diff;

    delete[] d;
  }

  cout << "================ S U M M A R Y ================\n\n";

  float sum_ratio = 0.f;
  int sum_parallel = 0;
  int sum_serial = 0;
  cout << "\tParallel" << "\t\tSerial" << "\t\tRatio (P/S)"
       << "\t\tDifference\n";
  cout << "  ----------------------------------------------------------------\n";
  for (unsigned i=0; i!=num_trials; i++) {
    cout << "\t" << duration_parallel[i] << " msec"
         << "\t\t" << duration_serial[i] << " msec"
         << "\t\t" << float(duration_parallel[i])/float(duration_serial[i])
         << "\t\t" << difference_parallel_serial[i]
         << endl;
    sum_parallel += duration_parallel[i];
    sum_serial += duration_serial[i];
    sum_ratio += float(duration_parallel[i])/float(duration_serial[i]);
  }
  cout << "  ----------------------------------------------------------------\n";
  cout << "\t" << float(sum_parallel)/float(num_trials) << " msec"
       << "\t\t" << float(sum_serial)/float(num_trials) << " msec"
       << "\t\t" << sum_ratio/float(num_trials)
       << endl;

  delete[] a;
  delete[] b;
  delete[] c;


  return 0;
}




@ OpenCL program.

@s kernel register
@s global register

@(kernel.cl@>=
kernel void vec_calc (global const float* a,
                      global const float* b,
                      global float* result
                      ) @+ {

  int id = get_global_id (0);

#undef __USE_CONDITIONAL__

#ifdef __USE_CONDITIONAL__

  if (sin(a[id]*cos(b[id])) > 0.) {
    result[id] = exp(sin(a[id])*cos(b[id]) + cos(a[id])*sin(b[id]));
  } else {
    result[id] = exp(sin(a[id])*cos(b[id]) - cos(a[id])*sin(b[id]));
  }

#else

  result[id] = exp(sin(a[id])*cos(b[id])
               +sign(sin(a[id])*cos(b[id]))*cos(a[id])*sin(b[id]));

#endif
}




@q ************************************************************************** @>
@q                                                                            @>
@q                                I N D E X                                   @>
@q                                                                            @>
@q ************************************************************************** @>
@* Index.  이 프로그램에 사용된 심볼과 그에 대한 설명을 보려면 아래의 인덱스를
참조하라.
